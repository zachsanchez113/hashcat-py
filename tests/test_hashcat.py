from __future__ import annotations

from hashcat.core.hashcat import Hashcat

# TODO: Update this one-by-one to check individual pieces of functionality
TEST_CASE_BASE = {
    "hash_type": "NTLM",  # TODO: alias for 'hash_mode'?
    "attack_mode": 0,
    "optimized_kernel_enable": True,
    "opencl_device_types": [2],
    "workload_profile": "Default",
    "status": True,
    "status_timer": 15,
}


def test_init():
    obj = Hashcat()
    del obj


# TODO: Test that config file is loaded correctly
def test_generate_cmd():
    cmd = Hashcat().generate_cmd(**TEST_CASE_BASE)
    print(f"\nOutput:\n\n{cmd}")

    # assert cmd == "/usr/local/bin/hashcat --optimized-kernel-enable --status --status-timer=15 --attack-mode=0 --hash-type=1000 --workload-profile=2 --opencl-device-types=2"

    assert "--hash-type" in cmd, "missing hash type"
    assert "--hash-type=1000" in cmd, "incorrect hash type"

    assert "--attack-mode" in cmd, "missing attack mode"
    assert "--attack-mode=0" in cmd, "incorrect attack mode"

    assert "--workload-profile" in cmd, "missing workload profile"
    assert "--workload-profile=2" in cmd, "incorrect workload profile"

    assert "--opencl-device-types" in cmd, "missing OpenCL device types"
    assert "--opencl-device-types=2" in cmd, "incorrect OpenCL device type"

    assert "--status " in cmd, "missing status"
    assert "--status-timer=15" in cmd, "missing status timer"
    assert "--optimized-kernel-enable" in cmd, "missing optimized kernel"
