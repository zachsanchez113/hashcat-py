# Test Data

This just holds test config and hashes as of now.

Passwords:

- password
- qwerty
- abc123
- dragon
- sunshine
- 654321
- master
- football
- 000000
- computer

To regenerate hashes, just download probable-wordlists, then:

```bash
head -n 20 probable-v2-top207.txt | grep -vP "^1" | xargs hashcat-py generate-hash ntlm
```
