"""Logging setup/constants."""

from __future__ import annotations

import os
import re
import sys
from textwrap import dedent

from loguru import logger

from hashcat.config import settings
from hashcat.util import walk_nested

LOG_FORMAT = """
    <green>{time:YYYY-MM-DD HH:mm:ss}</green> | <level>{level: <8}</level>
    | <cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - <level>{message}</level>
"""

LOG_FORMAT = re.sub(r"\n", "", dedent(LOG_FORMAT))  # pyright: ignore[reportConstantRedefinition]


def configure_logging(log_level: str = "", log_file: str = ""):
    """Configure logging with loguru.

    In order, log level/file is determined by:

    1. Function argument
    2. Environment variable
    3. Config file value

    If log level isn't found, a default of `INFO` is used.
    If log file isn't found, log messages will only be printed to stdout.

    Args:
        log_level (str, optional): Log level. Defaults to "".
        log_file (str, optional): Log file. Defaults to "".
    """

    if not log_level:
        if os.environ.get("HASHCAT_LOG_LEVEL"):
            log_level = os.environ["HASHCAT_LOG_LEVEL"].upper()
        elif walk_nested(settings, "base", "logging", "log_level"):
            log_level = walk_nested(settings, "base", "logging", "log_level").upper()
        else:
            log_level = "INFO"

    if not log_file:
        if os.environ.get("HASHCAT_LOG_FILE"):
            log_level = os.environ["HASHCAT_LOG_FILE"]
        elif walk_nested(settings, "base", "logging", "log_file"):
            log_level = walk_nested(settings, "base", "logging", "log_file").upper()

    # Configure handlers based on config
    handlers = [
        {
            "sink": sys.stdout,
            "level": log_level,
            "format": LOG_FORMAT,
        }
    ]

    if log_file:
        handlers.append(
            {
                "sink": log_file,
                "level": log_level,
                "format": LOG_FORMAT,
            }
        )

    # Clear existing loggers
    logger.remove()

    # Create handlers + custom coloring
    logger.configure(
        handlers=handlers,
        levels=[
            {"name": "INFO", "color": "<blue><bold>"},
            {"name": "DEBUG", "color": "<magenta><bold>"},
        ],
    )
