"""Abstraction layer for various hashcat 'options' like hash mode, attack mode, etc."""

from __future__ import annotations

import re
import subprocess
from collections import OrderedDict
from collections.abc import Generator
from typing import Any

import inflection
from attrs import Factory, define
from path import Path
from rich import box
from rich.console import Console, ConsoleOptions, RenderResult
from rich.panel import Panel
from rich.table import Table

from hashcat.config import settings
from hashcat.exceptions import HashcatBinaryError, HashcatValidationError
from hashcat.util import MutableNamedTuple, extract_hashcat_help_table


@define
class HashcatOptionMapping(MutableNamedTuple):
    """Make life a bit easier when it comes to validation."""

    # Make this available so exceptions are less verbose
    name: str

    def valid_choices(self):
        """Get all valid choices for this option mapping."""

        keys = [k.lower() if isinstance(k, str) else k for k in self.keys()]
        keys += [k if isinstance(k, str) else k for k in self.keys()]

        values = [v.lower() if isinstance(v, str) else v for v in self.values()]
        values += [v if isinstance(v, str) else v for v in self.values()]

        valid_choices = keys + values
        return valid_choices

    def is_valid(self, value: Any, raise_exception: bool = False) -> bool:
        """Check if the given value is a valid option."""

        if isinstance(value, list):
            return all(self.is_valid(v) for v in value)

        elif isinstance(value, str) and value.isdigit():
            value = int(value)

        if value not in self.valid_choices() and raise_exception:
            raise HashcatValidationError(
                f"Unknown {self.name} {value} specified!"
                + f" Please run 'hashcat-py show-options' to see all available choices"
            )

        return value in self.valid_choices()

    def convert(self, value: Any) -> Any:
        """Convert a name value to numeric (if needed)."""

        if self.is_valid(value):
            if isinstance(value, str) and value.isdigit():
                value = int(value)

            if isinstance(value, str):
                return self.get(value, "[ERROR]")
            else:
                return value

        else:
            return None

    def get(self, key: Any, default: Any = None) -> Any | None:
        for k, v in self.items():
            if k.lower() == key.lower():
                return v

        return default

    def keys(self) -> list[Any]:
        return list({k: v for k, v in self.items()}.keys())

    def values(self) -> list[Any]:
        return list({k: v for k, v in self.items()}.values())

    def items(self) -> Generator[tuple[Any, Any], None, None]:
        # Exclude anything that starts with underscore cause that's internal
        yield from {k: v for k, v in self.__dict__.items() if not k.startswith("_")}.items()

    def __str__(self):
        # Table view
        digits_max_len = max(len(str(opt)) for opt in self.values())
        return "\n".join(f"- {v:<{digits_max_len}} | {k}" for k, v in self.items())

    # TODO: Columns for hash mode?
    def __rich_console__(self, console: Console, options: ConsoleOptions) -> RenderResult:
        def check_lower(pair: tuple[str, str]):
            key, value = pair
            return (key.lower(), value)

        table = Table(box=box.SIMPLE_HEAVY)
        table.add_column("Mode", style="red", justify="right")
        table.add_column("Name")

        if self.name == "hash type":
            choices = OrderedDict(sorted(self.items(), key=check_lower))
        else:
            choices = dict(self.items())

        for k, v in choices.items():
            table.add_row(f"{v}", f"{k}")

        panel = Panel(
            table,
            title=f"[bold]{inflection.titleize(self.name)}s",
            subtitle="[bold]:light_bulb: Tip: You can also specify the name!",
            title_align="left",
            subtitle_align="left",
            border_style="blue",
        )

        yield panel


@define
class HashcatOptions:
    """Wrapper class for global self."""

    # TODO: HashcatOptionMapping for --backend-devices
    CLI_FLAGS: list[dict[str, Any]] = Factory(list)
    HASH_TYPES: HashcatOptionMapping = HashcatOptionMapping(name="hash type")
    OUTFILE_FORMATS: HashcatOptionMapping = HashcatOptionMapping(name="outfile format")
    ATTACK_MODES: HashcatOptionMapping = HashcatOptionMapping(name="attack mode")
    OPENCL_DEVICE_TYPES: HashcatOptionMapping = HashcatOptionMapping(name="OpenCL device type")
    WORKLOAD_PROFILES: HashcatOptionMapping = HashcatOptionMapping(name="workload profile")

    def generate_cli_flag(self, line: list[str]) -> dict[str, Any] | None:
        option, typeof, description, example = line

        short_option = ""
        long_option = ""
        if len(option.split(", ")) > 1:
            short_option, long_option = option.split(", ")
        else:
            long_option = option

        short_option = re.sub(r"^\-", "", short_option)
        long_option = re.sub(r"^\-\-", "", long_option)

        if long_option == "help":
            return

        if typeof == "Num":
            typeof = "number"
        elif typeof == "Char":
            typeof = "string"
        else:
            typeof = typeof.lower()

        if not description.endswith("."):
            description += "."

        return {
            "name": inflection.underscore(long_option),
            "short": short_option,
            "long": long_option,
            "help": description,
            "example": example,
            "required": False,
            "type": typeof,
            "is_flag": typeof == "",
            "multiple": long_option == "rules-file" or (typeof == "number" and "," in example),
        }

    def load(self) -> None:
        result = subprocess.run(
            f"{Path(settings.hashcat_bin).expand()} --help", shell=True, capture_output=True
        )

        if result.returncode != 0:
            raise HashcatBinaryError(f"Failed to extract hashcat's help page! ({result.returncode = })")

        lines = [line.strip() for line in result.stdout.decode().strip().split("\n")]

        for line in extract_hashcat_help_table(lines, "options"):
            if data := self.generate_cli_flag(line):
                self.CLI_FLAGS.append(data)

        # TODO: --advice-disable is undocumented, so check stderr for `hashcat: unrecognized option '--advice-disable'`
        self.CLI_FLAGS.append(
            {
                "name": "advice_disable",
                "short": "",
                "long": "advice-disable",
                "help": "Disable advice ",
                "example": "",
                "required": False,
                "type": "",
                "is_flag": True,
                "multiple": False,
            }
        )

        # TODO: This doesn't work right w/ hashcat v5, has lines like 'X | 1 = PBKDF2-HMAC-RIPEMD160 | Full-Disk Encryption (FDE)'
        self.HASH_TYPES.update(
            {
                line[1]: int(line[0]) if line[0].isdigit() else f"{i}__{line[0]}"
                for i, line in enumerate(extract_hashcat_help_table(lines, "hash modes"))
            }
        )

        self.OUTFILE_FORMATS.update(
            {line[1]: int(line[0]) for line in extract_hashcat_help_table(lines, "outfile formats")}
        )

        self.ATTACK_MODES.update(
            {line[1]: int(line[0]) for line in extract_hashcat_help_table(lines, "attack modes")}
        )

        for line in extract_hashcat_help_table(lines, "opencl device types"):
            number, dev_name = line
            if ", " in dev_name:
                self.OPENCL_DEVICE_TYPES.update({dev_name: int(number) for dev_name in dev_name.split(", ")})
            else:
                self.OPENCL_DEVICE_TYPES[dev_name] = int(number)

        self.WORKLOAD_PROFILES.update(
            {line[1]: int(line[0]) for line in extract_hashcat_help_table(lines, "workload profiles")}
        )
