"""Parse/ingest defaults based on a config file."""

from __future__ import annotations

import os
import sys
from shutil import which
from typing import Any, cast

import dynaconf.base
from appdirs import user_config_dir
from dynaconf import Dynaconf, Validator
from loguru import logger
from path import Path

from hashcat.util import walk_nested


def validate_dir(p: Any) -> bool:
    try:
        return Path(p).expand().isdir()
    except:
        return False


def validate_file(p: Any) -> bool:
    try:
        return Path(p).expand().isfile() and Path(p).expand().access(os.R_OK)
    except:
        return False


def validate_file_executable(p: Any) -> bool:
    try:
        return validate_file(p) and Path(p).expand().access(os.X_OK)
    except:
        return False


def validate_file_or_empty(p: Any) -> bool:
    try:
        return not p or validate_file(p)
    except:
        return False


def locate_hashcat_bin() -> str:
    """Determine where hashcat is based on PATH."""

    if sys.platform != "win32":
        hashcat_bin = which("hashcat", mode=os.X_OK) or "/usr/bin/hashcat"
    else:
        hashcat_bin = which("hashcat.exe", mode=os.X_OK) or "/usr/bin/hashcat"

    if not hashcat_bin:
        logger.error("Could not find hashcat installation!")
        logger.error(
            f"Please ensure hashcat is on your PATH, or specify a valid 'hashcat_bin' in your config file: {FULL_SETTINGS_PATH}"
        )
        sys.exit(1)

    return hashcat_bin


def load_settings(p: Path | None = None):
    """Load settings and, if needed, configure logging based on the default or specified config file.

    Args:
        p (Path | None, optional): Config file to load. Defaults to None.
    """

    from hashcat.logging import configure_logging

    old_log_level = walk_nested(settings, "base", "logging", "log_level")
    old_log_file = walk_nested(settings, "base", "logging", "log_file")

    if not p:
        # TODO: What's the load order?
        for f in SETTINGS_FILES:
            settings.load_file(f)
    else:
        settings.load_file(p.expand().abspath())

    log_level = walk_nested(settings, "base", "logging", "log_level")
    log_file = walk_nested(settings, "base", "logging", "log_file")

    if log_level != old_log_level or log_file != old_log_file:
        configure_logging(log_level=log_level, log_file=log_file)


SETTINGS_ROOT_PATH = Path(user_config_dir()).expand()
SETTINGS_FILES = ["hashcat.toml"]

# For debugging + cleaner error messages
FULL_SETTINGS_PATHS = [Path(SETTINGS_ROOT_PATH) / Path(SETTINGS_FILES[0])]

# TODO: What's the load order?
if sys.platform in ["linux", "darwin"]:
    SETTINGS_FILES.append("/etc/hashcat.toml")
    FULL_SETTINGS_PATHS.append(Path("/etc/hashcat.toml"))

FULL_SETTINGS_PATH = ", ".join(FULL_SETTINGS_PATHS)

logger.trace(f"{SETTINGS_ROOT_PATH = }")
logger.trace(f"{SETTINGS_FILES = }")
logger.trace(f"{FULL_SETTINGS_PATH = }")

# NOTE:
# - This is actually LazySettings, but you lose IntelliSense for methods if you don't coerce the type
# - For now, I'm only validating options that require a different type
settings: dynaconf.base.Settings = cast(
    dynaconf.base.Settings,
    Dynaconf(
        envvar_prefix="HASHCAT",
        root_path=SETTINGS_ROOT_PATH,
        settings_files=SETTINGS_FILES,
        validators=[
            Validator(
                "hashcat_bin",
                is_type_of=str,
                condition=validate_file_executable,
                default=locate_hashcat_bin(),
            ),
        ],
    ),
)
