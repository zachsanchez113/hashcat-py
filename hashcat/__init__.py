"""A Hashcat wrapper library to enable programmatic usage."""

from __future__ import annotations

from .logging import configure_logging

# Default logging setup while everything gets up and running
# TODO: Is this really a good idea?
configure_logging()

# Make CLI options globally available
from hashcat.options import HashcatOptions

options = HashcatOptions()
