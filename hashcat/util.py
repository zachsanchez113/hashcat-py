"""Utilities that don't have another home that makes sense (yet?).

TODO: Reorganize this into hashcat/utils/...
"""

from __future__ import annotations

import re
from collections.abc import Mapping, Sequence
from typing import Any

import dynaconf.base
import dynaconf.utils.functional
from passlib.hash import lmhash, nthash

from hashcat.exceptions import HashcatBinaryError


class Unreachable(Exception):
    """Indicates unreachable code.

    This is useful any time that the type checker can't determine that some code is unreachable.
    Of course, this also serves as a nice safeguard at runtime if a logical error is introduced by mistake.
    """


def walk_nested(
    obj: Sequence[Any] | Mapping[Any, Any] | dynaconf.utils.functional.LazyObject | dynaconf.base.Settings,
    *args: Any,
    default: Any = None,
    all_or_nothing: bool = True,
) -> Any:
    """Walk the nested objects inside 'obj' with the specified *args as key names and return the value at the end.

    This is slightly more flexible than obj.get('foo', {}).get('bar', {}).get('baz') because a TypeError can be thrown
    if the key name exists but is not a dictionary.

    Examples:
        obj = {'foo': {'bar': {'baz': 1}}, 'buzz': 'ERROR'}
        walk_nested(obj, 'foo', 'bar', 'baz')  # 1
        walk_nested(obj, 'foo', 'bar', 'buzz', all_or_nothing=False)  # {'baz': 1}
        walk_nested(obj, 'buzz', 'baz', all_or_nothing=False) # 'ERROR'

    Args:
        obj (Sequence | Mapping): A dictionary/iterable-like object containing nested dictionaries/elements.
        *args (Any): The individual keys and/or indices to walk.
        default (Any, optional): Default value to use if an exception occurs. Defaults to None.
        all_or_nothing (bool, optional): If True and an error is encountered, return the default. If False, return the most recent obj. Defaults to True.

    Returns:
        Any: The return value, determined per above.
    """

    try:
        for arg in args:
            obj = obj[arg]  # pyright: ignore[reportGeneralTypeIssues]
        return obj
    except (KeyError, TypeError, IndexError, AttributeError):
        return default if all_or_nothing else obj


def generate_lm_hash(password: str) -> str:
    """Generate the LM hash for a password.

    Args:
        password (str): Password to hash.

    Returns:
        str: LM hash.
    """

    return lmhash.hash(password)


def generate_ntlm_hash(password: str) -> str:
    """Generate the NTLM hash for a password.

    Args:
        password (str): Password to hash.

    Returns:
        str: NTLM hash.
    """

    return nthash.hash(password)


def extract_hashcat_help_table(lines: list[str], table_name: str) -> list[list[str]]:
    """Parse hashcat's help page to determine CLI options/accepted values for a given table/section.

    Args:
        lines (list[str]): Lines from 'hashcat --help' stdout.
        table_name (str): Table to parse.

    Raises:
        Exception: If the table couldn't be found.

    Returns:
        list[list[str]]: Lines from stdout, split by \\\\n.
    """

    if not lines:
        return []

    parts = []
    started = False
    i = 0
    for line in lines:
        if started:
            # Start of new table
            if re.match(r"^\- \[", line):
                break
            # Empty line, header line, divider
            elif i < 3:
                i += 1
                continue
            # Skip, obviously
            elif not line:
                continue
            # Split by delimiter
            else:
                parts.append([p.strip() for p in line.split("|")])

        if not line:
            continue
        if not started and re.match(rf"^\- \[ {table_name} \] \-", line, flags=re.IGNORECASE):
            started = True
            continue

    if not parts and not started:
        raise HashcatBinaryError(f"Couldn't find table {table_name!r}!")

    return parts


class MutableNamedTuple:
    """This is a quick attempt at a mutable NamedTuple that can take + update arbitrary attrs.

    There are dict-like methods as well if desired.

    TODO: I imagine there's a proper library for this, but I didn't feel like digging too much.
    """

    def __init__(self, **kwargs: Any) -> None:
        self.__dict__.update(kwargs)

    def get(self, key: Any, default: Any = None) -> Any | None:
        return self.__dict__.get(key, default)

    def update(self, other: Any) -> None:
        self.__dict__.update(other)

    def keys(self) -> list[Any]:
        return list(self.__dict__.keys())

    def values(self) -> list[Any]:
        return list(self.__dict__.values())

    def __getitem__(self, attr: Any) -> Any:
        if self.__dict__.get(attr):
            return self.__dict__.get(attr, None)
        else:
            raise KeyError(attr)

    def __setitem__(self, attr: Any, value: Any) -> None:
        self.__dict__[attr] = value

    def __getattr__(self, attr: Any) -> Any:
        if self.__dict__.get(attr):
            return self.__dict__.get(attr)
        else:
            raise AttributeError(f"{self.__class__.__name__!r} object has no attribute {attr!r}")

    def __setattr__(self, attr: Any, value: Any) -> Any:
        if attr != "__dict__":
            if hasattr(self, "__dict__"):
                self.__dict__[attr] = value
        else:
            object.__setattr__(self, attr, value)

    def __repr__(self) -> str:
        dict_str = ", ".join(f"{k!r}={v!r}" for k, v in self.__dict__.items())
        return f"{self.__class__.__name__}({dict_str})"

    def __str__(self) -> str:
        dict_str = ", ".join(f"{k!r}={v!r}" for k, v in self.__dict__.items())
        return f"{self.__class__.__name__}({dict_str})"
