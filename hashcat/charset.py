"""[WIP] Abstraction layer for hashcat charsets"""

from __future__ import annotations

from enum import Enum


class Charset(Enum):
    lowercase = ("?l", "abcdefghijklmnopqrstuvwxyz")
    uppercase = ("?u", "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    digits = ("?d", "0123456789")
    special = ("?s", r""" !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~""")

    hex_lowercase = ("?h", "0123456789abcdef")
    hex_uppercase = ("?H", "0123456789ABCDEF")
    bytes = ("?b", "0x00 - 0xff")

    hashtag = (r"\#", "#")
    comma = (r"\,", ",")
    question_mark = ("??", "?")

    def __str__(self):
        return self.value[0]

    def chars(self):
        return self.value[1]

    # TODO: could be better (still? need to test @property decorator)
    @property
    def all(cls):
        return (
            "?a",
            cls.lowercase.chars() + cls.uppercase.chars() + cls.digits.chars() + cls.special.chars(),
        )
