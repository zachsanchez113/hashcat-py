"""Exceptions raised by this library."""

from __future__ import annotations


class HashcatError(Exception):
    """Base exception type for this library."""


class HashcatBinaryError(HashcatError):
    """Error that occurred when running the hashcat binary."""


class HashcatValidationError(HashcatError):
    """A hashcat option/config value failed validation."""


class HashcatNotImplementedError(HashcatError):
    """A feature is unsupported for some reason."""
