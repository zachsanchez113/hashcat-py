"""Main hashcat runner."""

from __future__ import annotations

import re
import subprocess
import sys
from typing import Any, cast

import inflection
import pytomlpp as toml
from loguru import logger
from path import Path

from hashcat import options
from hashcat.config import load_settings, settings
from hashcat.exceptions import HashcatNotImplementedError, HashcatValidationError

MASK_FULL8 = "?a" * 8
MASK_HUMAN8 = r"""?l?u?d._!-@* #/$&\,+=)(??" "'" ";]"""


class Hashcat:
    """Hashcat wrapper class.

    Attributes:
        config (HashcatConfig): Default configuration/parameters for crackjobs.
    """

    def __init__(self, config_file: str = "", **defaults: Any):
        """Initialize hashcat wrapper class.

        Warning:
            Supplying a config file only overwrites preexisting config values - it doesn't remove any
            if a new value isn't supplied.

        Args:
            config_file (str, optional): Config file location. Defaults to "".
            defaults: Anything else you could put into the config file. Overrides config file.
        """

        # TODO: This could be an issue if being used as a library
        if config_file:
            logger.debug(f"Loading config file: {config_file}")
            load_settings(Path(config_file))

        if defaults:
            for k, v in defaults.items():
                logger.debug(f"Default: {k} : {v}")
                settings[k] = v

    def generate_cmd(self, **kwargs: Any) -> str:
        """Generate hashcat command for cracking.

        Note:
            To save on verbosity and ensure accurate preprocessing, only `kwargs` is used in the
            function definition. Each of the kwargs in this docstring are either custom or not
            explicit options in hashcat's CLI, so do take note of them.

            For everything else, refer to the help page of hashcat - the `kwargs` are automatically
            determined by the *long options* available in your hashcat installation, which differ
            by the version installed/specified in the configuration.

        Warning:
            Beware that this docstring's kwargs may be different from what's available to you!
            For validation purposes, kwargs are dynamically derived from hashcat's verbose options, so
            they will vary depending on the version you have installed.

        Args:
            hashcat_bin (str | path.Path, optional): Location of hashcat binary if not in config or PATH. Defaults to '/usr/local/bin/hashcat'.
            hashcat_utils_dir (str | path.Path, optional): Directory containing hashcat utils if not in config or PATH. Defaults to ''.
            config (str | path.Path, optional): Config file. Defaults to None.
            crackjob (str | path.Path, optional): Crackjob file. Defaults to None.
            log_file (str | path.Path, optional): Capture hashcat's stdout. Defaults to None.
            tee_log_file (str | path.Path, optional): Tee output to log instead of fully suppressing stdout. Defaults to False.
            hash_file (str | path.Path, optional): File containing hashes. Defaults to None.
            mask (str, optional): User-defined mask. Defaults to None.
            wordlists (list[str], optional): Files containing password candidates. Defaults to [].
            charsets (list[str], optional): User-defined charset(s). Defaults to [].

        Raises:
            Exception: If validation fails.

        Returns:
            str: The hashcat command to run.
        """

        logger.trace("Beginning command generation...")

        # Prepare options via the commandline
        options.load()

        # Apply defaults here
        for k, v in cast(dict[str, Any], settings.get("HASHCAT", {})).items():
            if (k := k.lower()) not in kwargs and "_bin" not in k and "_dir" not in k:
                logger.debug(f"Default: {k} : {v}")
                kwargs[k] = v

        # Core kwargs we're interested in
        hash_type = kwargs.pop("hash_type", None)
        attack_mode = kwargs.pop("attack_mode", None)
        rules_file = kwargs.pop("rules_file", None)
        charsets = kwargs.pop("charsets", None)
        hash_file = kwargs.pop("hash_file", "")
        mask = kwargs.pop("mask", None)
        wordlists: str | list[str] | None = kwargs.pop("wordlists", None)
        workload_profile = kwargs.pop("workload_profile", None)
        outfile_format = kwargs.pop("outfile_format", None)
        opencl_device_types = kwargs.pop("opencl_device_types", None)
        log_file = kwargs.pop("log_file", None)
        tee_log_file = kwargs.pop("tee_log_file", False)

        # Sanity checks + conversions for dual types
        # TODO: Expand charset validation?
        if charsets and len(charsets) > 4:
            raise HashcatValidationError(
                f"{len(charsets)} charsets were specified, but only a maximum of 4 may be supplied."
            )

        if attack_mode is not None and options.ATTACK_MODES.is_valid(attack_mode, raise_exception=True):
            kwargs["attack_mode"] = options.ATTACK_MODES.convert(attack_mode)

        if hash_type is not None and options.HASH_TYPES.is_valid(hash_type, raise_exception=True):
            kwargs["hash_type"] = options.HASH_TYPES.convert(hash_type)

        if workload_profile and options.WORKLOAD_PROFILES.is_valid(workload_profile, raise_exception=True):
            kwargs["workload_profile"] = options.WORKLOAD_PROFILES.convert(workload_profile)

        if outfile_format:
            if not isinstance(outfile_format, list):
                outfile_format = [outfile_format]

            kwargs["outfile_format"] = [
                options.OUTFILE_FORMATS.convert(fmt)
                for fmt in outfile_format
                if options.OUTFILE_FORMATS.is_valid(fmt, raise_exception=True)
            ]

        if opencl_device_types:
            if not isinstance(opencl_device_types, list):
                opencl_device_types = [opencl_device_types]

            kwargs["opencl_device_types"] = [
                options.OPENCL_DEVICE_TYPES.convert(dev)
                for dev in opencl_device_types
                if options.OPENCL_DEVICE_TYPES.is_valid(dev, raise_exception=True)
            ]

        # Convert easier CLI options
        hashcat_cli_options = []

        if charsets:
            hashcat_cli_options.append(
                " ".join(f"-{i + 1} '{charset}'" for i, charset in enumerate(charsets))
            )

        if rules_file:
            hashcat_cli_options.append(" ".join(f"--rules-file={r}" for r in rules_file))

        # For the rest, just roll with what we were provided
        for option, v in kwargs.items():
            long_option = f"--{inflection.dasherize(option)}"

            # Sanity check to make sure a) user didn't mess something up,
            # and b) I didn't mess up options and/or their processing
            if not any(option == opt["name"] for opt in options.CLI_FLAGS):
                raise HashcatValidationError(f"Unknown option '{long_option}' found in kwargs!")
            else:
                if v is None:
                    logger.warning(f"Option {long_option!r} is null, skipping...")
                    continue

                if isinstance(v, list):
                    v = ",".join([str(v_) for v_ in v])

                if not isinstance(v, (str, int)):
                    continue

                if not isinstance(v, bool):
                    if v == "":
                        raise HashcatValidationError(
                            f"Expected a value for {long_option!r}, but no value was provided."
                        )
                    else:
                        hashcat_cli_options.append(f"{long_option}={v}")

                elif v is True:
                    hashcat_cli_options.append(long_option)

        hashcat_cli_options = " ".join(hashcat_cli_options)

        # Process non-option CLI arguments
        if wordlists and isinstance(wordlists, list):
            wordlists = " ".join(str(w) for w in wordlists)

        # Hybrid mask + wordlist needs special ordering
        if attack_mode := kwargs.get("attack_mode"):
            candidates = f"{mask} {wordlists}" if attack_mode == 7 else f"{wordlists} {mask}"
        else:
            candidates = wordlists or ""

        # And now we're at the end result
        cmd = f"{settings.hashcat_bin} {hashcat_cli_options} {hash_file} {candidates}"

        # TODO: Add Windows support
        if log_file:
            if sys.platform in ["linux", "darwin"]:
                if tee_log_file:
                    cmd += f" | tee {log_file}"
                else:
                    cmd += f" >> {log_file}"
            else:
                raise HashcatNotImplementedError(f"Logfiles are currently unsupported for Windows.")

        # Remove extraneous whitespace, cause that's annoying
        cmd = re.sub(r"\s{2,}", " ", cmd).strip()

        return cmd

    def load_crackjob(self, filename: str) -> str:
        """Helper to generate the hashcat command for a given crackjob config.

        Args:
            filename (str): Crackjob config.

        Returns:
            str: Hashcat command.
        """

        with open(filename) as f:
            job_config = toml.load(f)

        return self.generate_cmd(**job_config.get("base", {}), **job_config.get("options", {}))

    def crack(self, crackjob: str = "", hashcat_cmd: str = "", *args: Any, **kwargs: Any) -> bool:
        """Crack some passwords.

        Args:
            crackjob (str, optional): Crackjob config to load values from. Defaults to "".
            hashcat_cmd (str, optional): Hashcat command to run if not using crackjob config. Defaults to "".
            *args, **kwargs: If no hashcat command was provided, these are passed to `generate_cmd()`.

        Returns:
            bool: Success?
        """

        if crackjob:
            self.load_crackjob(crackjob)
        elif not hashcat_cmd:
            hashcat_cmd = self.generate_cmd(*args, **kwargs)

        result = subprocess.run(hashcat_cmd, shell=True)
        return result.returncode in [0, 1]
