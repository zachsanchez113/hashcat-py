"""CLI entrypoint."""

from __future__ import annotations

import rich_click as click
from loguru import logger

from .crack import crack
from .hashes import generate_hash
from .help import help
from .info import info
from .util import LoguruCatchGroup

# Config for rich-click
click.rich_click.STYLE_HELPTEXT = ""
click.rich_click.SHOW_ARGUMENTS = True
click.rich_click.SHOW_METAVARS_COLUMN = False
click.rich_click.APPEND_METAVARS_HELP = True
click.rich_click.STYLE_OPTIONS_TABLE_PAD_EDGE = True
click.rich_click.STYLE_COMMANDS_TABLE_PAD_EDGE = True
click.rich_click.OPTION_GROUPS = {
    "hashcat-py crack": [
        {
            "name": "Configuration",
            "options": [
                "--config",
                "--crackjob",
                "--hashcat-bin",
                "--hashcat-utils-dir",
            ],
        },
    ],
}


@click.group(cls=LoguruCatchGroup, context_settings=dict(help_option_names=["-h", "--help"]))
@logger.catch()
def cli():
    """Hashcat wrapper script designed to make command line usage a bit cleaner."""


cli.add_command(crack)
cli.add_command(info)
cli.add_command(help)
cli.add_command(generate_hash)
