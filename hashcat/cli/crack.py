"""CLI commands for core cracking functionality."""

from __future__ import annotations

import os
from typing import Any

import rich_click as click
from loguru import logger
from path import Path
from rich.panel import Panel
from rich.syntax import Syntax

from hashcat import options
from hashcat.cli.util import DEFAULT_CLICK_DIR, DEFAULT_CLICK_FILE, generate_hashcat_click_options
from hashcat.config import settings
from hashcat.core.hashcat import Hashcat
from hashcat.logging import configure_logging

console = click.rich_click._get_rich_console()


# TODO: Add extra validation from hashcat, or at least add a note to the description? See
# `user_options_getopt`, `user_options_sanity`, and `outer_loop`
@click.command(
    no_args_is_help=True,
    context_settings=dict(
        show_default=True,
        ignore_unknown_options=True,
        help_option_names=["-h", "--help"],
    ),
)
@click.argument(
    "HASH_FILE",
    required=False,
    nargs=1,
    type=DEFAULT_CLICK_FILE,
)
@click.argument(
    "wordlists_or_mask",
    metavar="[WORDLIST(s)] [MASK]",
    required=False,
    nargs=-1,
)
@click.option(
    "--config",
    "config_file",
    help="Config file.",
    type=DEFAULT_CLICK_FILE,
)
@click.option(
    "--crackjob",
    help="Crackjob file.",
    type=DEFAULT_CLICK_FILE,
)
@click.option(
    "--hashcat-bin",
    help="Location of hashcat binary if not in config or PATH.",
    type=DEFAULT_CLICK_FILE,
    default=settings.get("hashcat_bin", "/usr/bin/hashcat"),
    show_default=True,
)
@click.option(
    "--log-file",
    help="Logfile for crackjob output.",
    type=click.Path(dir_okay=False, resolve_path=True, path_type=Path),
)
@click.option(
    "--tee",
    "tee_log_file",
    is_flag=True,
    help="Tee output to log instead of fully suppressing stdout.",
)
@click.option(
    "--hashcat-utils-dir",
    help="Directory containing hashcat utils if not in config or PATH.",
    type=DEFAULT_CLICK_DIR,
    default=settings.get("hashcat_utils_dir", "/usr/bin/"),
    show_default=True,
)
@click.option(
    "--dry-run",
    is_flag=True,
    help="Print the command that would be executed.",
)
@click.option(
    "--debug",
    is_flag=True,
    help="Enable debug logging.",
)
@generate_hashcat_click_options()
def crack(**kwargs: Any):
    """Run hashcat."""

    if debug := kwargs.pop("debug", None):
        configure_logging(log_level="DEBUG")

    dry_run = kwargs.pop("dry_run", False)

    # Prepare options via the commandline
    options.load()

    # Remove all the extraneous stuff
    kwargs = {k: v for k, v in kwargs.items() if v is not None}

    if kwargs.get("hash_info"):
        raise click.ClickException(f"Usage of '--hash-info' isn't supported at this very second")
    if kwargs.get("backend_info"):
        raise click.ClickException(f"Usage of '--backend-info' isn't supported at this very second")

    # Set up the core kwargs for Hashcat wrapper
    hashcat_kwargs = {}
    if kwargs.get("hashcat_bin"):
        hashcat_kwargs["hashcat_bin"] = kwargs.pop("hashcat_bin")
    if kwargs.get("hashcat_utils_dir"):
        hashcat_kwargs["hashcat_utils_dir"] = kwargs.pop("hashcat_utils_dir")
    if kwargs.get("config_file"):
        config_file = kwargs.pop("config_file")
        hashcat_kwargs["config_file"] = config_file

    # Set up + validate kwargs for crackjob (if found)
    wordlists: list[str] = []
    mask: str = ""

    wordlists_or_mask = kwargs.pop("wordlists_or_mask", [])
    for elem in wordlists_or_mask:
        if "?" in elem:
            mask = elem
        else:
            if not Path(elem).isfile():
                raise click.BadParameter(f"File '{elem}' does not exist.")
            elif not Path(elem).access(os.R_OK):
                raise click.BadParameter(f"File '{elem}' is not readable.")
            else:
                wordlists.append(elem)

    if wordlists:
        kwargs["wordlists"] = wordlists
    if mask:
        kwargs["mask"] = mask

    if debug:
        logger.debug("hashcat_kwargs:")
        console.print_json(data=hashcat_kwargs, indent=2, sort_keys=True)

        logger.debug("kwargs for generate_cmd:")
        console.print_json(data=kwargs, indent=2, sort_keys=True)

    hashcat = Hashcat(**hashcat_kwargs)

    if job_file := kwargs.pop("job_file", None):
        logger.debug(f"Loading crackjob: {job_file.expand()}")
        cmd = hashcat.load_crackjob(job_file.expand())
    else:
        cmd = hashcat.generate_cmd(**kwargs)

    if dry_run:
        panel = Panel(
            Syntax(cmd, lexer="bash", word_wrap=True),
            title="[bold]command",
            title_align="left",
            style="green",
        )

        console.print("", panel)

    else:
        hashcat.crack(hashcat_cmd=cmd)
