"""CLI commands for generating various hashes."""

from __future__ import annotations

import rich_click as click

from hashcat.util import generate_lm_hash, generate_ntlm_hash

console = click.rich_click._get_rich_console()


@click.group(no_args_is_help=True, context_settings=dict(help_option_names=["-h", "--help"]))
def generate_hash():
    """Generate the hash for one or more passwords."""


@generate_hash.command(no_args_is_help=True, context_settings=dict(help_option_names=["-h", "--help"]))
@click.argument("passwords", type=str, nargs=-1)
def lm(passwords: list[str]):
    """Generate LM hash(es)."""

    for password in passwords:
        print(generate_lm_hash(password))


@generate_hash.command(no_args_is_help=True, context_settings=dict(help_option_names=["-h", "--help"]))
@click.argument("passwords", type=str, nargs=-1)
def ntlm(passwords: list[str]):
    """Generate NTLM hash(es)."""

    for password in passwords:
        print(generate_ntlm_hash(password))
