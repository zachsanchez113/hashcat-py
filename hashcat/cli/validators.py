"""Functions for click option validation"""

from __future__ import annotations

import sys
from functools import partial
from typing import Any

import inflection
import rich_click as click
from rich.console import Group
from rich.panel import Panel

from hashcat import options
from hashcat.options import HashcatOptionMapping


# TODO: integrate all this w/ core?
# TODO: >1 valid option
def __validate_cli(
    ctx: click.Context,
    param: click.core.Option,
    arg: Any,
    options_map: HashcatOptionMapping,
) -> int | None:
    """Validate param from CLI.

    Args:
        ctx (Any): Ignored.
        param (click.core.Option): Parameter passed in as part of Click's validation.
        arg (Any): What was passed in.
        options (HashcatOptionMapping): Valid options for the given param.

    Raises:
        click.BadParameter: If the value provided isn't valid.

    Returns:
        int | None: Provided value if a valid int, or corresponding int if valid string.
    """

    # Prepare options via the commandline
    options.load()

    if isinstance(param, click.core.Option):
        param_name: str = inflection.humanize(param.to_info_dict().get("name", "")).lower()
    else:
        param_name: str = param

    if arg is None:
        return arg

    if options_map.is_valid(arg):
        return options_map.convert(arg)
    else:
        console = click.rich_click._get_rich_console()

        if ctx is not None:
            console.print(ctx.get_usage())
        if click.rich_click.ERRORS_SUGGESTION:
            console.print(click.rich_click.ERRORS_SUGGESTION, style=click.rich_click.STYLE_ERRORS_SUGGESTION)
        elif (
            click.rich_click.ERRORS_SUGGESTION is None
            and ctx is not None
            and ctx.command.get_help_option(ctx) is not None
        ):
            console.print(
                f"Try [blue]'{ctx.command_path} {ctx.help_option_names[0]}'[/] for help.",
                style=click.rich_click.STYLE_ERRORS_SUGGESTION,
            )

        base_error_text = click.rich_click.highlighter(
            f"\nUnknown {param_name} {arg!r} specified.\n\nPlease select from the options below:\n"
        )

        if "hash" not in param_name:
            subtitle = None
            group = Group(base_error_text, options_map)
        else:
            # Hash type error is super long, so let's add the error at the bottom of the panel
            subtitle = f"{click.rich_click.ERRORS_PANEL_TITLE}: Unknown {param_name} {arg!r} specified."

            # Add a trailing whitespace to account for the subtitle
            group = Group(base_error_text, options_map, "")

        # No whitespace after usage looks pretty bad imo
        console.print()
        console.print(
            Panel(
                group,
                border_style=click.rich_click.STYLE_ERRORS_PANEL_BORDER,
                title=click.rich_click.ERRORS_PANEL_TITLE,
                title_align=click.rich_click.ALIGN_ERRORS_PANEL,
                subtitle=subtitle,
                subtitle_align=click.rich_click.ALIGN_ERRORS_PANEL,
            )
        )

        if click.rich_click.ERRORS_EPILOGUE:
            console.print(click.rich_click.ERRORS_EPILOGUE)

        sys.exit(1)


cli_validate_hash_type = partial(__validate_cli, options_map=options.HASH_TYPES)
cli_validate_attack_mode = partial(__validate_cli, options_map=options.ATTACK_MODES)
cli_validate_workload = partial(__validate_cli, options_map=options.WORKLOAD_PROFILES)
cli_validate_opencl_device_types = partial(__validate_cli, options_map=options.OPENCL_DEVICE_TYPES)
cli_validate_outfile_format = partial(__validate_cli, options_map=options.OUTFILE_FORMATS)
