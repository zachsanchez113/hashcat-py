"""CLI commands for pretty-printing hashcat options."""

from __future__ import annotations

import contextlib

import rich_click as click

from hashcat import options

console = click.rich_click._get_rich_console()


@click.group(context_settings=dict(help_option_names=["-h", "--help"]))
def help():
    """Show available options for various hashcat parameters.

    NOTE: Both numerical values AND names are valid. All names are case-insensitive, so
    `hashcat-py crack -m sha2-256` and `hashcat-py crack -m SHA2-256` would both do the same thing.
    """

    options.load()


@help.command(context_settings=dict(help_option_names=["-h", "--help"]))
def attack_mode():
    """Get information about available attack modes."""

    console.print(options.ATTACK_MODES)


@help.command(context_settings=dict(help_option_names=["-h", "--help"]))
@click.option("--no-pager", is_flag=True, help="Disable the pager even if output is too big for screen.")
def hash_type(no_pager: bool = False):
    """Get information about available hash types."""

    with contextlib.nullcontext() if no_pager else console.pager(pager=None, styles=True):
        console.print(options.HASH_TYPES)


@help.command(context_settings=dict(help_option_names=["-h", "--help"]))
def opencl_device_type():
    """Print table of OpenCL device types."""

    console.print(options.OPENCL_DEVICE_TYPES)


@help.command(context_settings=dict(help_option_names=["-h", "--help"]))
def outfile_format():
    """Print table of outfile formats."""

    console.print(options.OUTFILE_FORMATS)


@help.command(context_settings=dict(help_option_names=["-h", "--help"]))
def workload_profile():
    """Print table of workload profiles."""

    console.print(options.WORKLOAD_PROFILES)
