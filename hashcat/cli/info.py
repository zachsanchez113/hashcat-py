"""CLI commands for detailed info/help on hashcat options/features."""

from __future__ import annotations

import contextlib
import json
import re
import subprocess
from textwrap import dedent
from typing import Any

import inflection
import rich_click as click
from attrs import Factory, define
from path import Path
from rich import box
from rich.console import Console, ConsoleOptions, RenderResult
from rich.panel import Panel
from rich.table import Table

from hashcat import options
from hashcat.cli.validators import cli_validate_attack_mode, cli_validate_hash_type
from hashcat.config import settings

console = click.rich_click._get_rich_console()


@define
class Hash:
    """Information about an individual hash mode in hashcat.

    Provides a prettier representation than hashcat by using the `rich` library.
    """

    hash_mode: str
    hash_info: dict[str, Any] = Factory(dict)

    def add_row(self, table: Table) -> None:
        """Add this hash's information to a `rich` table.

        In hindsight, I don't think I implemented this functionality in the correct place, but I don't feel like
        rewriting this function to work in the context of `HashInfo` right now.
        """

        password_tup = f"[{self.hash_info.get('password_len_min')}, {self.hash_info.get('password_len_max')}]"

        if self.hash_info.get("is_salted"):
            salt_range = f"[{self.hash_info.get('salt_len_min')}, {self.hash_info.get('salt_len_max')}]"
        else:
            salt_range = "N/A"

        table.add_row(
            self.hash_mode,
            self.hash_info.get("name"),
            self.hash_info.get("category"),
            password_tup,
            self.hash_info.get("salt_type", "N/A"),
            salt_range,
            ", ".join(self.hash_info.get("kernel_type") or []),
        )

    def __rich_console__(self, console: Console, options: ConsoleOptions) -> RenderResult:
        table = Table(box=box.SIMPLE_HEAVY)
        table.add_column("Mode", style="red", justify="right")
        table.add_column("Name")
        table.add_column("Category")
        table.add_column("Password.Len")
        table.add_column("Salt.Type")
        table.add_column("Salt.Len")
        table.add_column("Kernel")

        self.add_row(table)

        panel = Panel(
            table,
            title=f"[bold]Hash Mode: {self.hash_mode}",
            subtitle="[bold]:light_bulb: Tip: You can also specify the name!",
            title_align="left",
            subtitle_align="left",
            border_style="blue",
        )

        yield panel


@define
class HashInfo:
    """Information about all of the hash modes available in hashcat.

    Provides a prettier representation than hashcat by using the `rich` library and sorting hash modes by name
    instead of by number.
    """

    hash_info: dict[str, Any] = Factory(dict)

    def get(self, key: Any) -> Hash:
        key = str(key)
        return Hash(key, self.hash_info[key])

    def __rich_console__(self, console: Console, options: ConsoleOptions) -> RenderResult:
        table = Table(box=box.SIMPLE_HEAVY)
        table.add_column("Mode", style="red", justify="right")
        table.add_column("Name")
        table.add_column("Category")
        table.add_column("Password.Len")
        table.add_column("Salt.Type")
        table.add_column("Salt.Len")
        table.add_column("Kernel")

        # Make sure the table is sorted by name instead of number so it's easier to find what you need
        for hash_mode, data in sorted(self.hash_info.items(), key=lambda item: item[1]["name"]):
            Hash(hash_mode, data).add_row(table)

        panel = Panel(
            table,
            title=f"[bold]Hash Modes",
            subtitle="[bold]:light_bulb: Tip: You can also specify the name!",
            title_align="left",
            subtitle_align="left",
            border_style="blue",
        )

        yield panel


def parse_hash_info() -> dict[str, dict[str, Any]]:
    """Extract hash information out of hashcat's JSON.

    TODO: Support for specific hash types, e.g. `hashcat --hash-info -m 1000`
    """

    hash_info = {}

    # Try and get JSON out the gate
    # NOTE: This is a newer feature (didn't work for v6.2.5)
    result = subprocess.run(
        f"{Path(settings.hashcat_bin).expand()} --hash-info --machine-readable",
        shell=True,
        capture_output=True,
    )

    if result.returncode == 0:
        # Who needs JSON standards anyway...
        lines = result.stdout.decode().split("\n")[2:]
        hash_info: dict[str, dict[str, Any]] = json.loads("".join(lines))

    # Gotta parse out the hash info tables if the current version doesn't support JSON
    else:
        result = subprocess.run(
            f"{Path(settings.hashcat_bin).expand()} --hash-info",
            shell=True,
            capture_output=True,
        )

        if result.returncode != 0:
            raise click.ClickException(
                f"Failed to run '{Path(settings.hashcat_bin).expand()} --hash-info'! (code: {result.returncode})"
                + f"\n\n{result.stderr.decode()}"
            )

        else:
            hash_info_sections = [s.split("\n ") for s in result.stdout.decode().split("\n\n") if s]
            for section in hash_info_sections:
                hash_mode = re.sub(r"Hash mode #", "", section[0], flags=re.IGNORECASE)

                # We'll get the header otherwise
                if not hash_mode or not hash_mode.isdigit():
                    continue

                hash_info[hash_mode] = {}

                for line in section[1:]:
                    key, value = re.split(r"[\.]{1,}: ", line.strip(), maxsplit=2)

                    if key == "Kernel.Type(s)":
                        key = "kernel_type"
                        value = value.split(",")
                    elif key == "Plaintext.Encoding":
                        key = "plaintext_encoding"
                        value = value.split(",")
                    else:
                        key = re.sub(r"\(s\)", "", key.lower().replace(".", "_"))

                    hash_info[hash_mode][key] = value

                hash_info[hash_mode]["is_salted"] = hash_info[hash_mode].get("salt_type") is not None

    return hash_info


@click.group(context_settings=dict(help_option_names=["-h", "--help"]))
def info():
    """Show detailed information about various hashcat parameters and features."""

    options.load()


@info.command(context_settings=dict(help_option_names=["-h", "--help"]))
@click.argument("attack_mode", type=str, required=False, callback=cli_validate_attack_mode)
@click.option("-v", "--verbose", is_flag=True, help="Show detailed information about the hash type(s).")
def attack_mode(attack_mode: str, verbose: bool = False):
    """Get information about available attack modes."""

    attack_modes = [{"name": k, "mode": v} for k, v in options.ATTACK_MODES.items()]

    if not verbose and not attack_mode:
        console.print(options.ATTACK_MODES)
        return

    for d in attack_modes:
        name = d.get("name") or "[ERROR]"
        mode = d.get("mode")

        # Straight is 0
        if mode is None:
            mode = "[ERROR]"

        description = ""
        examples = []

        if name.lower() in ["straight", "dictionary"]:
            description = """
                Used for dictionary attacks, which involve trying to crack a hash with a list of password
                candidates.

                Optionally, you may also apply rules (transformations such as appending '123') to each
                candidate.
            """

            examples = [
                f"""
                    # Basic usage
                    hashcat -m 0 -a {mode} hashes.txt [bold green]wordlist.txt[/bold green]
                """,
                f"""
                    # Apply a set of rules to each word
                    hashcat -m 0 -a {mode} [bold blue]-r rules.txt[/bold blue] hashes.txt [bold green]wordlist.txt[/bold green]
                """,
            ]

        elif name.lower() in ["combination", "combinator"]:
            description = """
                Used for combinator attacks, which combine two wordlists into one.

                Note that you cannot apply a rules file in this attack type - you may only supply singular
                rules that are applied to either the left or the right wordlist.
            """

            examples = [
                f"""
                    # Basic usage
                    hashcat -m 0 -a {mode} hashes.txt [bold green]wordlist1.txt[/bold green] [bold blue]wordlist2.txt[/bold blue]
                """,
                f"""
                    # Append '-' to each word in wordlist1.txt before combining
                    hashcat -m 0 -a {mode} [bold green]--rule-left='$-'[/bold green] hashes.txt [bold green]wordlist1.txt[/bold green] [bold blue]wordlist2.txt[/bold blue]
                """,
                f"""
                    # Append '!' to each word in wordlist2.txt before combining
                    hashcat -m 0 -a {mode} [bold blue]--rule-right='$!'[/bold blue] hashes.txt [bold green]wordlist1.txt[/bold green] [bold blue]wordlist2.txt[/bold blue]
                """,
                f"""
                    # Append '-' to each word in wordlist1.txt and '!' to each word in wordlist2.txt before combining
                    hashcat -m 0 -a {mode} [bold green]--rule-left='$-'[/bold green] [bold blue]--rule-right='$!'[/bold blue] hashes.txt [bold green]wordlist1.txt[/bold green] [bold blue]wordlist2.txt[/bold blue]
                """,
            ]

        elif name.lower() in ["brute-force", "mask"]:
            description = """
                Try all combinations from a given keyspace, which may be one of the built-in character sets
                or a custom one.

                This attack mode can also accept charset files (e.g. add characters specific to German), as
                well as "mask files" that can specify multiple masks.
            """

            examples = [
                f"""
                    # Brute-force combinations of length 8 for all lowercase letters
                    # Example: aaaaaaaa, aaaaaaab, ..., abaaagcf, ..., hdyvhxwb, ...
                    hashcat -m 0 -a {mode} [bold green]?l?l?l?l?l?l?l?l[/bold green] hashes.txt
                """,
                f"""
                    # Brute-force combinations of a word + 4 digits
                    # Example: [bold][blue]Spring[/blue][green]0000[/green][/bold], [bold][blue]Spring[/blue][green]0001[/green][/bold], ..., [bold][blue]Spring[/blue][green]9876[/green][/bold], ...
                    hashcat -m 0 -a {mode} [bold][blue]Spring[/blue][green]?d?d?d?d[/green][/bold] hashes.txt
                """,
                f"""
                    # Brute-force combinations of length 8 with a custom charset ('abcde')
                    # Example: aaaaa, aaaab, ..., aaaae, ..., abcde, baaaa, ... bbcde, ...
                    hashcat -m 0 -a {mode} [bold][blue]--custom-charset1=abcde[/blue] [green]?1?1?1?1?1?1?1?1[/green][/bold] hashes.txt
                """,
                f"""
                    # Brute-force combinations of lengths 1-8 across the entire character space
                    # Example: a, aa, aaa, ..., b, bb, ...
                    hashcat -m 0 -a {mode} [bold][blue]--increment[/blue] [green]?a?a?a?a?a?a?a?a[/green][/bold] hashes.txt
                """,
                f"""
                    # Use an hcmask file (format of each line in the file is the same as for the command line)
                    hashcat -m 0 -a {mode} hashes.txt [bold green]mask_file.hcmask[/bold green]
                """,
            ]

        elif "hybrid" in name.lower():
            if "+ mask" in name.lower():
                description = f"""
                    Hybrid attack modes are essentially a combinator attack, but with a wordlist and a mask
                    instead of two wordlists.

                    Here, the mask will be appended to each word in the wordlist.
                """

                examples = [
                    f"""
                        # Append all four digit combinations to each word in the wordlist
                        # Example: [bold][green]Fall[/green][blue]1347[/blue][/bold]
                        hashcat -m 0 -a {mode} [bold][green]wordlist.txt[/green] [blue]?d?d?d?d[/blue][/bold]
                    """,
                    f"""
                        # Append all digits 20XX to each word in the wordlist
                        # Example: [bold][green]Fall[/green][yellow]20[/yellow][blue]11[/blue][/bold]
                        hashcat -m 0 -a {mode} [bold][green]wordlist.txt[/green] [yellow]20[/yellow][blue]?d?d[/blue][/bold]
                    """,
                ]

            else:
                description = f"""
                    Hybrid attack modes are essentially a combinator attack, but with a wordlist and a mask
                    instead of two wordlists.

                    Here, the mask will be prepended to each word in the wordlist.
                """

                examples = [
                    f"""
                        # Prepend all four digit combinations to each word in the wordlist
                        # Example: [bold][blue]1347[/blue][green]Fall[/green][/bold]
                        hashcat -m 0 -a {mode} [bold][blue]?d?d?d?d[/blue] [green]wordlist.txt[/green][/bold]
                    """,
                ]

        elif "association" in name.lower():
            description = """
                Tries each word in a single wordlist against a single hash. It is used when a likely password
                or password component is already known (e.g. username), correlated with each target hash.

                See: https://hashcat.net/forum/thread-9534.html
            """

        d["description"] = dedent(description).strip().replace("\n", " ").replace("  ", "\n\n")
        d["examples"] = "\n\n".join(dedent(example).strip() for example in examples) + "\n"

    # TODO: More precise padding
    table = Table(
        box=box.SIMPLE_HEAVY,
        padding=1,
    )
    for k in attack_modes[0].keys():
        table.add_column(inflection.titleize(k))

    for d in attack_modes:
        vals = [str(v) for v in d.values()]
        table.add_row(*vals)

    panel = Panel(
        table,
        title=f"[bold]Attack Modes",
        subtitle="[bold]:light_bulb: Tip: You can also specify the name!",
        title_align="left",
        subtitle_align="left",
        border_style="deep_sky_blue1",
    )

    console.print(panel)


@info.command(context_settings=dict(help_option_names=["-h", "--help"]))
@click.argument("hash_mode", type=str, required=False, callback=cli_validate_hash_type)
@click.option("-v", "--verbose", is_flag=True, help="Show detailed information about the hash type(s).")
@click.option("--no-pager", is_flag=True, help="Disable the pager even if output is too big for screen.")
def hash_type(hash_mode: str, verbose: bool = False, no_pager: bool = False):
    """Get information about available hash types."""

    hash_mode = options.HASH_TYPES.convert(hash_mode)
    hash_info = parse_hash_info()

    if hash_mode:
        if verbose:
            console.print(HashInfo(hash_info).get(hash_mode))
        # TODO
        else:
            console.print_json(data=hash_info.get(str(hash_mode)))

    else:
        with contextlib.nullcontext() if no_pager else console.pager(pager=None, styles=True):
            if verbose:
                console.print(HashInfo(hash_info))
            else:
                console.print(options.HASH_TYPES)
