"""Utility functions for error handling and click option generation + parsing.

TODO: Move this to hashcat/utils/cli.py
"""

from __future__ import annotations

import sys
from typing import Any, Callable

import path
import rich_click as click
from loguru import logger

from hashcat import options
from hashcat.cli.validators import cli_validate_attack_mode, cli_validate_hash_type, cli_validate_workload

# Prevent verboseness
DEFAULT_CLICK_FILE = click.Path(
    exists=True,
    file_okay=True,
    dir_okay=False,
    resolve_path=True,
    path_type=path.Path,
)

DEFAULT_CLICK_DIR = click.Path(
    exists=True,
    file_okay=False,
    dir_okay=True,
    resolve_path=True,
    path_type=path.Path,
)


class LoguruCatchGroup(click.RichGroup):
    """Subclass of RichGroup that wraps commands in loguru's `logger.catch` context manager."""

    def __call__(self, *args: Any, **kwargs: Any):
        with logger.catch(onerror=lambda e: sys.exit(1), exclude=(click.ClickException, click.Abort)):
            return super().__call__(*args, **kwargs)


def append_charset(ctx: click.Context, param: click.core.Option, arg: Any) -> Any:
    """Callback to group charset-related CLI flags into a single kwarg

    Args:
        ctx (Any): Ignored.
        param (click.core.Option): Parameter passed in as part of Click's validation.
        arg (Any): What was passed in.

    Returns:
        Any: arg itself
    """

    if arg is not None:
        if not isinstance(ctx.params.get("charsets"), list):
            ctx.params["charsets"] = []

        ctx.params["charsets"].append(arg)

    return arg


def split_click_option(ctx: click.Context, param: click.core.Option, arg: Any) -> list[int] | None:
    """Turn comma-delimited string into a list and validate each element if possible.

    Args:
        ctx (Any): Ignored.
        param (click.core.Option): Parameter passed in as part of Click's validation.
        arg (Any): What was passed in.

    Returns:
        list[int] | None: Provided value(s) as a list.
    """

    if arg is None:
        return arg

    if isinstance(arg, str):
        args = [int(i) if i.isdigit() else i for i in arg.split(",")]
    else:
        args = []
        for a in arg:
            args.extend(a.split(","))

    # Dynamically determine validation function from globals
    param_name = param.to_info_dict().get("name", "")

    if (validator := globals().get(f"cli_validate_{param_name}")) and callable(validator):
        for i, arg in enumerate(args):
            args[i] = validator(ctx, param, arg)

    # TODO: Bad reason? Ignore return type issues since validator returns None when called directly w/ arg=None
    return list(set(args))  # pyright: ignore[reportGeneralTypeIssues]


# TODO: Handle --hash-info
def generate_hashcat_click_options() -> Callable[..., Any]:
    """Click option wrapper to dynamically build hashcat-py CLI options."""

    map_to_types = dict(
        array=str,
        number=int,
        string=str,
        file=click.Path(exists=True, resolve_path=True, path_type=path.Path),
    )

    def decorator(f: Callable[..., Any]) -> Callable[..., Any]:
        # Prepare options via the commandline
        options.load()

        for opt_params in sorted(options.CLI_FLAGS, key=lambda f: f.get("name", ""), reverse=True):
            if opt_params.get("short"):
                param_decls = ("-" + opt_params["short"], "--" + opt_params["long"], opt_params["name"])
            else:
                param_decls = ("--" + opt_params["long"], opt_params["name"])

            # TODO: Always using 'multiple=True' may be a bit too permissive
            # NOTE: 'default=None' is important since config file will be overriden by a value of, say, 'False'
            attrs: dict[str, Any] = dict(
                default=None,
                required=opt_params.get("required", False),
                type=map_to_types.get(opt_params.get("type", "string"), None),
                help=opt_params.get("help", ""),
                is_flag=opt_params.get("is_flag", False),
                multiple=opt_params.get("multiple", False),
            )

            # Special options with dedicated callbacks
            if opt_params.get("short") in ["1", "2", "3", "4"]:
                attrs["callback"] = append_charset

            if opt_params.get("name") == "hash_type":
                attrs.update(
                    {
                        "type": str,
                        "help": "Hash type (run hashcat-py show-options to see available choices).",
                        "callback": cli_validate_hash_type,
                    }
                )

            if opt_params.get("name") == "attack_mode":
                attrs.update(
                    {
                        "type": str,
                        "help": "Attack mode (run hashcat-py show-options to see available choices).",
                        "callback": cli_validate_attack_mode,
                    }
                )

            if opt_params.get("name") == "opencl_device_types":
                attrs.update(
                    {
                        "type": str,
                        "help": "OpenCL device types to use (run hashcat-py show-options to see available choices).",
                        "multiple": True,
                        "callback": split_click_option,
                    }
                )

            if opt_params.get("name") == "outfile_format":
                attrs.update(
                    {
                        "type": str,
                        "help": "Outfile format (run hashcat-py show-options to see available choices).",
                        "multiple": True,
                        "callback": split_click_option,
                    }
                )

            if opt_params.get("name") == "workload_profile":
                attrs.update(
                    {
                        "type": str,
                        "help": "Workload profile (run hashcat-py show-options to see available choices).",
                        "callback": cli_validate_workload,
                    }
                )

            click.option(*param_decls, **attrs)(f)

        return f

    return decorator
