from __future__ import annotations

from hashcat.cli import crack


def generate_cmd_docstring_args():
    """I'm not typing this out by hand."""

    lines = []

    # Pull CLI flags to save some effort
    for param in crack.params:
        # Helps us get the stuff we need w/o doing weird ops
        param_info = param.to_info_dict()

        name = param_info["name"]
        type = param_info["type"]["param_type"]
        default = param_info["default"]
        help_text = param_info.get("help", "MISSING").rstrip(".")
        is_flag = param_info.get("is_flag", False)
        multiple = param_info.get("multiple", False)

        if name == "hash_file":
            help_text = "File containing hashes"

        # Since I'm cheating by pulling CLI flags, I need to see if there's a callback that provides
        # a hint to the actual type since it defaults to str
        callback = param.callback
        if callback is not None:
            if hasattr(callback, "__name__") and callback.__name__ == "split_click_option":
                type = "list[str | int]"
            elif hasattr(callback, "func") and callback.func.__name__ == "__validate_cli":
                type = "str | int"

        # Convert to actual Python types
        if type == "Path":
            type = "str | path.Path"
        elif type == "String":
            type = "str"
        elif type == "Float":
            type = "int"

        if is_flag:
            type = "bool"

        if multiple and "List" not in type:
            type = f"list[{type}]"

        # Take care of these later since implementation is kinda strange
        if name not in [
            "custom_charset1",
            "custom_charset2",
            "custom_charset3",
            "custom_charset4",
            "wordlists_or_mask",
        ]:
            lines.append(f"{name} ({type}, optional): {help_text}. Defaults to {default!r}.")

    lines.append("mask (str, optional): User-defined mask. Defaults to None.")
    lines.append("wordlists (list[str], optional): Files containing password candidates. Defaults to [].")
    lines.append("charsets (list[str], optional): User-defined charset(s). Defaults to [].")
    lines.append("help (bool, optional): Print help. Defaults to False.")

    # Needs some manual cleanup after
    print("\n".join(lines))


def generate_cmd_stub():
    """I'm not typing this out by hand."""

    kwargs = []

    # Pull CLI flags to save some effort
    for param in crack.params:
        # Helps us get the stuff we need w/o doing weird ops
        param_info = param.to_info_dict()

        name = param_info["name"]
        type = param_info["type"]["param_type"]
        default = param_info["default"]
        is_flag = param_info.get("is_flag", False)
        multiple = param_info.get("multiple", False)

        # Since I'm cheating by pulling CLI flags, I need to see if there's a callback that provides
        # a hint to the actual type since it defaults to str
        callback = param.callback
        if callback is not None:
            if hasattr(callback, "__name__") and callback.__name__ == "split_click_option":
                type = "List[Union[str, int]]"
            elif hasattr(callback, "func") and callback.func.__name__ == "__validate_cli":
                type = "Union[str, int]"

        # Convert to actual Python types
        if type == "Path":
            type = "Union[str, path.Path]"
        elif type == "String":
            type = "str"
        elif type == "Float":
            type = "int"

        if is_flag:
            type = "bool"

        if multiple and "List" not in type:
            type = f"List[{type}]"

        if default is None:
            type = f"Optional[{type}]"

        # Take care of these later since implementation is kinda strange
        if name not in [
            "custom_charset1",
            "custom_charset2",
            "custom_charset3",
            "custom_charset4",
            "wordlists_or_mask",
        ]:
            kwargs.append(f"{name}: {type} = ...,")

    kwargs.append("mask: str | None = ...,")
    kwargs.append("wordlists: list[str] = ...,")
    kwargs.append("charsets: list[str] = ...,")
    kwargs.append("help: bool = ...,")

    kwargs = "\n    ".join(kwargs)
    print(f"def generate_cmd(\n    self,\n    {kwargs},\n    **kwargs,\n) -> str: ...")
