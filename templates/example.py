from __future__ import annotations

from hashcat.core.hashcat import MASK_FULL8, Hashcat

# Just making some stuff up
DEFAULT_OPTS = {
    "wordlists": ["/usr/share/cracking/wordlists/rockyou.txt"],
    "rules_files": ["/usr/share/cracking/rules/dive.rule"],
    "runtime": 60,
    "log_file": "/var/log/hashcat.log",
}

hash_files = ["/tmp/hashes/1.txt", "/tmp/hashes/2.txt"]

# Initialize hashcat
hashcat = Hashcat(python_bin="/home/zach/.pyenv/versions/3.9.5/bin/python")

# Run wordlist + rules over all hash files
for f in hash_files:
    hashcat_cmd = hashcat.generate_cmd(hash_type=111, attack_mode=0, hash_file=f, **DEFAULT_OPTS)
    hashcat.crack(hashcat_cmd=hashcat_cmd)

# Run brute-force over all hashes
# This puts together commands semi-blindly, so, just in case
DEFAULT_OPTS.update({"rules_files": [], "mask": MASK_FULL8})

for f in hash_files:
    # If you want, you can ignore the `generate_cmd()` bit for any method
    hashcat.crack(hash_type=111, attack_mode=3, hash_file=f, **DEFAULT_OPTS)
