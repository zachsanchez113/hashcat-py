# hashcat-py

This is a wrapper around [hashcat](https://hashcat.net/wiki/doku.php?id=hashcat>), a few [hashcat-utils](https://hashcat.net/wiki/doku.php?id=hashcat_utils), and [rephraser.py](https://github.com/travco/rephraser) so that programmatic usage of hashcat is easier to achieve.

## Getting Started

1. Ensure you've installed `poetry` with `pip install poetry`.
2. (Optionally) Create a virtual environment if you don't want to use `poetry`'s.
3. Clone this repo and run `poetry install`.
4. Activate the virtual environment with `poetry shell` if you didn't create one earlier. Alternatively, any commands/scripts can be run with `poetry run`, e.g. `poetry run hashcat-py --help`.

### Config file

You can place global or site-level defaults in a TOML config file.

This config file may be located in the following places on Linux:

- `~/.config/hashcat.toml`
- `/etc/hashcat.toml`

And on Windows:

- `C:\Users\<username>\AppData\Local\hashcat.toml`

At a bare minimum, you should define the following keys:

```toml
hashcat_bin = ""
hashcat_utils_dir = ""
prince_bin = ""
python_bin = ""
rephraser_bin = ""
```

## CLI interface

There's a CLI interface you can use by running `hashcat-py` (or `python -m hashcat`).

- To view available commands, run `hashcat-py --help`.
- To view the arguments/parameters for any commands, run `hashcat-py <command> --help`.
  - This may reveal subcommands in some cases.

## Base Hashcat Class

Initialize this with the paths of `hashcat_bin`, `prince_bin`, `python_bin`, and `rephraser_bin` as needed.

**WARNING:** If the `hashcat` binary isn't found or executable, an exception will be thrown.

```
hashcat = Hashcat(hashcat_bin="/usr/local/bin/hashcat")
```

### `generate_cmd`

Used to generate a hashcat command. Arguments are derived from verbose flags for `hashcat`,
but if anything extra is needed that I didn't implement, `extra_hashcat_opts` is available for you
to supply straight command-line argumnets.

Note that there's minimal checks to determine if everything will play together nicely - that's a bit
complicated to implement, and I don't wanna spin my wheels on it. So the onus is on you to make sure
everything works correctly.

Each of these arguments can also be passed to any of the cracking functions if you don't wanna deal
with the extra step of generating a command.

#### Parameters

There are a few custom parameters to take note of:

```
hashcat_bin (str | path.Path, optional): Location of hashcat binary if not in config or PATH. Defaults to '/usr/local/bin/hashcat'.
hashcat_utils_dir (str | path.Path, optional): Directory containing hashcat utils if not in config or PATH. Defaults to ''.
config (str | path.Path, optional): Config file. Defaults to None.
crackjob (str | path.Path, optional): Crackjob file. Defaults to None.
log_file (str | path.Path, optional): Capture hashcat's stdout. Defaults to None.
tee_log_file (str | path.Path, optional): Tee output to log instead of fully suppressing stdout. Defaults to False.
hash_file (str | path.Path, optional): File containing hashes. Defaults to None.
mask (str, optional): User-defined mask. Defaults to None.
wordlists (list[str], optional): Files containing password candidates. Defaults to [].
charsets (list[str], optional): User-defined charset(s). Defaults to [].
```

Beyond that, they're the exact same parameters as hashcat, just in snake_case.

### `crack`

Normal crackjob.

#### Parameters

`hashcat_cmd` (`str`, optional): Hashcat command to run. Defaults to "".
`*args`, `**kwargs`: If no hashcat command was provided, these are passed to `generate_cmd()`.
